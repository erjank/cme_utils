import numpy as np
import matplotlib.pyplot as plt


def get_data(log_file):
    try:
        data = np.genfromtxt(log_file)
        return data
    except (ValueError, IOError):
        print("Problem with this log file")
        print(log_file)
        print("\n")
        return None


def get_data_with_headers(log_file):
    try:
        data = np.genfromtxt(log_file, comments="@", names=True)
        return data
    except (ValueError, IOError):
        print("Problem with this log file")
        print(log_file)
        print("\n")
        return None


def make_ebar_plot(x, y, y_err):
    plt.errorbar(x, y, y_err)


def make_plot(x, y):
    plt.plot(x, y, 'ro')
