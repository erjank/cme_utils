#!/opt/local/bin/python
import sys
import argparse
import os
import hoomd
import hoomd.deprecated
from hoomd import md
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats


def get_bond_distances(snapshot,bond_type):
    #print('box length', snapshot.box.Lx)
    b_distances = []
    l_pos = snapshot.particles.position
    bonds = snapshot.bonds.group
    btypes = snapshot.bonds.types
    btypeIds = snapshot.bonds.typeid
    if hasattr(snapshot, 'box'):
        sim_box = snapshot.box
        Lx = sim_box.Lx
        Ly = sim_box.Ly
        Lz = sim_box.Lz
    elif hasattr(snapshot, 'configuration'):
        sim_box = snapshot.configuration.box
        Lx = np.float64(sim_box[0])
        Ly = np.float64(sim_box[1])
        Lz = np.float64(sim_box[2])

    print('box length', Lx)
    # print(ptypes,btypes)
    for i, bond in enumerate(bonds):
        if btypes[btypeIds[i]] == bond_type:
            p1 = l_pos[bond[0]]
            p2 = l_pos[bond[1]]
            dx1 = p1[0] - p2[0]
            dx2 = p1[1] - p2[1]
            dx3 = p1[2] - p2[2]
            if abs(dx1) > Lx / 2:
                dx1 = abs(dx1) - Lx
            if abs(dx2) > Ly / 2:
                dx2 = abs(dx2) - Ly
            if abs(dx3) > Lz / 2:
                dx3 = abs(dx3) - Lz

            dist = np.sqrt((dx1) ** 2 + (dx2) ** 2 + (dx3) ** 2)

            b_distances.append(dist)
    return b_distances

def get_bond_angles(snapshot,angle_type):
    #print('box length', snapshot.box.Lx)
    b_angles = []
    l_pos = snapshot.particles.position
    angles = snapshot.angles.group
    atypes = snapshot.angles.types
    atypeIds = snapshot.angles.typeid
    if hasattr(snapshot, 'box'):
        sim_box = snapshot.box
        Lx = sim_box.Lx
        Ly = sim_box.Ly
        Lz = sim_box.Lz
    elif hasattr(snapshot, 'configuration'):
        sim_box = snapshot.configuration.box
        Lx = np.float64(sim_box[0])
        Ly = np.float64(sim_box[1])
        Lz = np.float64(sim_box[2])

    print('box length', Lx)
    # print(ptypes,btypes)
    for i, angle in enumerate(angles):
        if atypes[atypeIds[i]] == angle_type:
            a = l_pos[angle[0]]
            b = l_pos[angle[1]]
            c = l_pos[angle[2]]
            ba = a - b
            bc = c - b

            cosine_angle = np.dot(ba, bc) / (np.linalg.norm(ba) * np.linalg.norm(bc))
            angle = np.arccos(cosine_angle)
            angle = np.rad2deg(angle)
            b_angles.append(angle)
    return b_angles


def get_bond_distribution(b_distances):
    m, s = stats.norm.fit(b_distances)
    return m, s


def plot_bond_distribution(file_path, bond_type):
    hoomd.context.initialize('--mode=cpu')
    if file_path.endswith('.hoomdxml'):
        system = hoomd.deprecated.init.read_xml(file_path)
    elif file_path.endswith('.gsd'):
        system = hoomd.init.read_gsd(file_path)
    snapshot = system.take_snapshot(bonds=True)
    b_distances = get_bond_distances(snapshot, bond_type)
    m, s = get_bond_distribution(b_distances)
    print('Mean: {}, Std: {}'.format(round(m, 2), round(s, 2)))

    # find minimum and maximum of xticks, so we know
    # where we should compute theoretical distribution
    #xt = plt.xticks()[0]
    #xmin, xmax = min(xt), max(xt)
    #lnspc = np.linspace(xmin, xmax, len(b_distances))

    #ab,bb,cb,db = stats.beta.fit(b_distances)
    #print(ab,bb,cb,db)
    #pdf_beta = stats.beta.pdf(bins, ab, bb,cb, db)
    #plt.plot(lnspc, pdf_beta, label="Beta")

    # lets try the normal distribution
    n, bins, patches = plt.hist(b_distances, 500, normed=1, alpha=0.75, label='{} bond lengths'.format(bond_type))
    pdf_g = stats.norm.pdf(bins, m, s) # now get theoretical values in our interval
    plt.plot(bins, pdf_g, label="Mean: {}, Std: {}".format(round(m,2),round(s,2))) # plot it

    plt.xlabel(r"$r$")
    plt.ylabel('Count')
    #plt.tight_layout()
    plt.legend()
    #plt.xlim(0,2.5)

    plt.show()

def plot_angle_distribution(file_path, angle_type):
    hoomd.context.initialize('--mode=cpu')
    if file_path.endswith('.hoomdxml'):
        system = hoomd.deprecated.init.read_xml(file_path)
    elif file_path.endswith('.gsd'):
        system = hoomd.init.read_gsd(file_path)
    snapshot = system.take_snapshot(bonds=True)
    b_angles = get_bond_angles(snapshot, angle_type)
    m, s = get_bond_distribution(b_angles)
    print('Mean: {}, Std: {}'.format(np.round(m, 2), np.round(s, 2)))

    # find minimum and maximum of xticks, so we know
    # where we should compute theoretical distribution
    # xt = plt.xticks()[0]
    # xmin, xmax = min(xt), max(xt)
    # lnspc = np.linspace(xmin, xmax, len(b_distances))

    # ab,bb,cb,db = stats.beta.fit(b_distances)
    # print(ab,bb,cb,db)
    # pdf_beta = stats.beta.pdf(bins, ab, bb,cb, db)
    # plt.plot(lnspc, pdf_beta, label="Beta")

    # lets try the normal distribution
    n, bins, patches = plt.hist(b_angles, 500, normed=1, alpha=0.75, label='{} bond lengths'.format(angle_type))
    pdf_g = stats.norm.pdf(bins, m, s)  # now get theoretical values in our interval
    plt.plot(bins, pdf_g, label="Mean: {}, Std: {}".format(round(m, 2), round(s, 2)))  # plot it

    plt.xlabel(r"$\theta$")
    plt.ylabel('Count')
    # plt.tight_layout()
    plt.legend()
    # plt.xlim(0,2.5)

    plt.show()

def main():
    parser = argparse.ArgumentParser(description='Takes in a hoomdxml files to generate bond length distribution of a bond type')
    parser.add_argument("-x", "--hoomd_xml", help="this file provides bond information, ie a hoomdxml file or gsd file")
    parser.add_argument("-b", "--bond_type", help="this string provides the bond type, e.g. 'C-C'")
    parser.add_argument("-a", "--angle_type", help="this string provides the bond angle type, e.g. 'C-C-C'")
    args = parser.parse_args()
    if args.hoomd_xml == '' or args.hoomd_xml is None:
        print('Please provide hoomd_xml')
        return

    init_file_name=args.hoomd_xml

    if args.bond_type is not None and args.bond_type != '':
        bond_type = args.bond_type
        plot_bond_distribution(init_file_name,bond_type)
    else:
        print('Not plotting bond distance histogram because no bond type was provided using -b')
    if args.angle_type is not None and args.angle_type != '':
        angle_type = args.angle_type
        plot_angle_distribution(init_file_name, angle_type)
    else:
        print('Not plotting bond angle histogram because no bond angle type was provided using -a')
