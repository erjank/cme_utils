import numpy

def autocorr1D(array):
    '''Takes in a linear numpy array, performs autocorrelation
       function and returns normalized array with half the length
       of the input'''
    ft = numpy.fft.rfft(array-numpy.average(array))
    acorr = numpy.fft.irfft(ft*numpy.conjugate(ft))/(len(array)*numpy.var(array))
    return acorr[0:int(len(acorr)/2)]


def different(a1,s1,a2,s2):
	if a2>a1:
		a1,s1,a2,s2 = a2,s2,a1,s1  #a1 holds larger value
	if (a1-s1)>(a2+s2):
		return True
	return False


def find_equilibrated_window(t,pe):
	rpe = pe[::-1] #reverse view into pe
	rt = t[::-1]
	startT = []
	for sample_size in [5, 10, 20, 50,100]:
		n_samples = int(len(rpe)/sample_size)
		firstA = numpy.mean(rpe[:sample_size])
		firstS= numpy.std(rpe[:sample_size])
		for i in range(1,n_samples):
			newA = numpy.mean(rpe[i*sample_size:(i+1)*sample_size])
			newS = numpy.std(rpe[i*sample_size:(i+1)*sample_size])
			if different(newA, newS, firstA, firstS):
				startT.append(int(rt[i*sample_size]))
				break
	if startT == []:
		 return 0,0
	med = numpy.median(startT)
	for start in range(len(t)):
		if t[start] >= med:
			return start, t[start]


def autocorrelation(log_file='log', log_write_time=100000, start_t = 0, dcd_write_time=1e6):
    logdat = numpy.loadtxt(log_file, skiprows=1)
    start_index = int(start_t/log_write_time)
    print("starting at index", start_index)
    t = logdat[start_index:,0]
    t = t-t[0]
    dt = t[1]-t[0]
    pe = logdat[start_index:,1]
    start, startT = find_equilibrated_window(t,pe)

    t = t[start:]
    pe = pe[start:]
    acorr = autocorr1D(pe)
    for acorr_i in range(len(acorr)):
        if acorr[acorr_i]<0:
            break
    lags = [i*dt for i in range(len(acorr))]

    frameStart = int(t[0]/dcd_write_time)
    frameStride = int(lags[acorr_i]/dcd_write_time)
    if frameStride == 0:
        frameStride = 1
    f = open('sampling.txt','w')
    nsamples = (int(t[-1]/dcd_write_time)-frameStart)/frameStride
    temps = "There are %.5e steps, (" % t[-1]
    temps = temps + "%d" % int(t[-1]/dcd_write_time)
    temps = temps + " frames)\n"
    temps = temps + "You can start sampling at t=%.5e" % t[0]
    temps = temps + " (frame %d)" % int(t[0]/dcd_write_time )
    temps = temps + " for %d samples\n" % nsamples
    temps = temps + "Because the autocorrelation time is %.5e" % lags[acorr_i]
    temps = temps + " (%d frames)\n" % int(lags[acorr_i]/dcd_write_time)
#    f.write(temps+"\n")
    f.close()
    return [frameStart, frameStride, nsamples]

