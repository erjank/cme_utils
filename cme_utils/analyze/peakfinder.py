import sys
import numpy
import scipy.ndimage as ndi
from skimage.feature import peak_local_max

def dpix_to_q(dpix):
    r = sigma*zoom*L/(dpix)
    q = 2*numpy.pi/r
    return q


    zoom = 8  # let's add zoom, L, and Sigma to the gui, as well as rotation.
    L = 30.017  # In order for q and r to be correct, it is SUPER important that L is correct
    sigma = 3.55
    around = 20
    angle = 0#64.29

    def dpix_to_q(dpix):
        r = sigma*zoom*L/(dpix)
        q = 2*numpy.pi/r
        return q


    def q_to_dpix(q):
        dpix = (q*sigma*zoom*L)/(2*numpy.pi) - 1
        return round(dpix)  # bankers rounding in 3.x, 'standard' in 2.7

def draw_circle(radius):
    # radius must be in pixles
    circle1 = plt.Circle((h, w), radius, color='r', fill=False)
    ax.add_artist(circle1)


def update(val):
    global img  # Silly hack since I can't return anything with this function :-(
    angle = s_angle.val  # Image rotated. #TODO: Ensure rotation about origin.
    img = ndi.interpolation.rotate(img_orig, angle, reshape=False, order=1)
    imgplt.set_data(img)


def maxnear(image, x, y, around):
    view = image[y-around:y+around, x-around:x+around]
    c = peak_local_max(view, min_distance=10)
    if len(c) < 1:
        print("No local maximum")
        return None
    return c[0]


def click(event):
    sigma = s_sigma.val
    L = s_L.val
#   zoom = s_zoom.val
    if event.inaxes == ax_angle:
        return
    elif event.inaxes == ax:
        x = int(event.xdata)
        y = int(event.ydata)
        c = maxnear(img, x, y, around)
        if c is None:
            return
        y = y-around+c[0]
        x = x-around+c[1]
        dpix = (int(x - w)**2. + int(y - h)**2.)**0.5
        if dpix == 0.:
            r = L*sigma
            q = 0.
        else:
            r = sigma*zoom*L/(dpix)
            q = 2*numpy.pi/r
        print("x={} y={} d={} q={} r={} I={}".format(x, y, dpix, q, r, img[y, x]))

if __name__ == "__main__":
    import matplotlib
    matplotlib.use('TkAgg')
    import matplotlib.pyplot as plt
    import matplotlib.image as mpimg
    from matplotlib.widgets import Slider

    zoom = 4  # let's add zoom, L, and Sigma to the gui, as well as rotation.
    L =  39.72 # In order for q and r to be correct, it is SUPER important that L is correct
    sigma = 3.905
    around = 20
    angle = 0

    print("Welcome! This thing prints out the info corresponding to the local maximum nearest where you click, not precisely where you click")
    print("Don't trust the numbers once you rotate the figure")

    infile = sys.argv[1]

    #Mikes debugging values
    #L = 30.01734161377 #In order for q and r to be correct, it is SUPER important that L is correct
    #sigma = 3.211136176

    fig, ax = plt.subplots()

    plt.subplots_adjust(left=0.1, bottom=0.2)
    img_orig = mpimg.imread(infile)

    img = ndi.interpolation.rotate(img_orig, angle, reshape=False)
    h, w = int(img.shape[0]/2)-1, int(img.shape[1]/2)-1
    imgplt = plt.imshow(img, cmap = plt.cm.get_cmap('jet'))

    #draw_circle(q_to_dpix(1.76))
    #draw_circle(q_to_dpix(0.29))

    axcolor = 'lightgoldenrodyellow'

    ax_angle = plt.axes([.3, 0.93, 0.4, 0.03], axisbg=axcolor)
    ax_sigma = plt.axes([.08, 0.03, 0.3, 0.03], axisbg=axcolor)
    ax_L = plt.axes([.55, 0.03, 0.3, 0.03], axisbg=axcolor)
    #ax_zoom = plt.axes([.4, 0.03, 0.3, 0.04], axisbg=axcolor) #This needs to be located better

    #TODO Once a good location for the zoom slider is found, we can add it.
    s_angle = Slider(ax_angle, 'rotate', -90, 90, valinit=angle)
    s_sigma = Slider(ax_sigma, '\u03C3', -0.5, 5, valinit=sigma)
    s_L = Slider(ax_L, 'L', 0, 20, valinit=L)
    #s_zoom = Slider(ax_zoom, 'Zoom', 0, 16, valinit=zoom) #TODO Zoom needs to be a factor of 2? 8? the slider needs to respect that

    s_angle.on_changed(update)
    fig.canvas.mpl_connect('button_press_event', click)
    plt.show()
    #This is where the figure is created, feel free to add tiles and lables as required

    sigma = s_sigma.val
    L = s_L.val
    #zoom = s_zoom.val
    fig_pp, ax_pp = plt.subplots()
    #img = img/(numpy.amax(img)-numpy.amin(img))-numpy.amin(img)/(numpy.amax(img)-numpy.amin(img))  # This renormizes our data after rotation to have the min be 0 and the max be 1
    #print(numpy.amin(img))
    #print(numpy.amax(img))
    imgplt_pp = ax_pp.imshow(img[:img.shape[0]//2, :], cmap = plt.cm.get_cmap('jet'), extent=[-dpix_to_q(w), dpix_to_q(w), 0, dpix_to_q(h)])
    plt.xticks(numpy.arange(-2.5, 3.0, 0.5))
    plt.setp(ax_pp.get_xticklabels()[::2], visible = False)
    plt.yticks(numpy.arange(0, 3.0, 0.5))
    plt.setp(ax_pp.get_yticklabels()[::2], visible = False)

    #imgplt_pp = ax_pp.imshow(img[:img.shape[0]//2, :], extent=[-dpix_to_q(w), dpix_to_q(w), 0, dpix_to_q(h)])

    ax_pp.set_xlabel(r"q$_{xy}[\AA^{-1}$]")
    ax_pp.set_ylabel(r"q$_{z} [\AA^{-1}$]")

    print("saving figure test.png")
    fig_pp.savefig("test.png")
