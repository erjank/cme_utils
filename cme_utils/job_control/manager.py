from os import listdir
from os import makedirs
from os import path
from os import getcwd
from subprocess import call
from subprocess import check_output
from datetime import datetime
import sqlite3
from pprint import pprint

columns = {'id':'INT','name':'TEXT','submitted':'TEXT','tstart':'TEXT','tstop':'TEXT','ttotal':'TEXT','comment':'TEXT','dir':'TEXT','tps':'REAL','hardware':'TEXT','crashed':'INT','new':'INT','continued':'INT','from_mav':'INT','pbsid':'TEXT','system':'TEXT','trusted':'INT','project':'TEXT','overwrote':'INT','parent':'INT','child':'INT','n':'INT'}#Evan added 'INT:'from_mav'
clusters = {'dirac_small':'dirac','dirac_reg':'dirac','gpu':'maverick','batch':'kestrel','MRI':'kestrel','rd':'rd','none':'none','quick':'kestrel','debug':'bw',
 'normal':'bw', 'virtual-local':'virtual-local'}
max_wall = {'kestrel':10000.,'rd':10000.,'dirac':'6.','maverick':12.,'none':24.,'bw':48.,
 'virtual-local':99999}

rnf = "rn.txt"

def write(filename,string):
    with open(filename,'w') as ts:
        ts.write(string)

class db_manager():
    def __init__(self,fn='sim_db.sqlite3'):
        self.filename=fn
        self.con = sqlite3.connect(self.filename)
        cur  = self.con.cursor()
        cur.execute("select name from sqlite_master where type='table' and name='runs';")
        if cur.fetchone() == None:
            self.create_db()

    def create_db(self):
        with self.con:
            print("Creating new table in database.")
            cur = self.con.cursor()
            s = 'create table runs('
            for k,v in columns.items():
                if k=='id':
                    s+='id INTEGER PRIMARY KEY, '
                else:
                    s+='{} {}, '.format(k,v)
            s = s[:-2]+' );'
            cur.execute(s)

    def add_run(self,name):
        with self.con:
            self.con.row_factory = sqlite3.Row
            cur = self.con.cursor()
            cur.execute("insert into runs(name) values ('{}');".format(name))
            self.lastrowid = cur.lastrowid
            cur.execute("select id from runs where rowid = {};".format(self.lastrowid))
            self.lastid = cur.fetchone()[0]
            return self.lastid

    def update(self,i,key,val):
        with self.con:
            cur=self.con.cursor()
            if key not in list(columns.keys()):
                print("key not found")
                return
            if columns[key]=='TEXT':
                cur.execute("update runs set {}='{}' where id={};".format(key,val,i))
            if columns[key] in ['INT','REAL']:
                cur.execute("update runs set {}={} where id={};".format(key,val,i))

    def get_id(self,key,value):
        with self.con:
            cur=self.con.cursor()
            if key not in list(columns.keys()):
                print("key not found")
                return
            if columns[key]=='TEXT':
                cur.execute("select id from runs where {}='{}';".format(key,value))
            if columns[key] in ['INT','REAL']:
                cur.execute("select id from runs where {}={};".format(key,value))
            a = cur.fetchone()
            if a != None:
                return a[0]

    def get(self,i,key):
        with self.con:
            cur=self.con.cursor()
            if key not in list(columns.keys()):
                print("key not found")
                return
            cur.execute("select {} from runs where id={};".format(key,i))
            a = cur.fetchone()
            if a != None:
                return a[0]

    def find_last(self):
        with self.con:
            cur=self.con.cursor()
            cur.execute("select id from runs")
            self.lastid = 0
            a = [i[0] for i in cur.fetchall()]
            if a==[]:
                return 0
            else:
                return max(a)

    def find_last_submitted(self):
        with self.con:
            cur=self.con.cursor()
            cur.execute("select pbsid from runs")
            return max( [int(i[0]) if i[0] != None else -1 for i in cur.fetchall()] )


class HoomdRun:
    """Class for managing hoomd runs."""
    def __init__(self, **kwargs):
        """Constructor.
            Args:
                **kwargs (dict): keyword arguments for a run that will be read, written, and used to initialize a run.
        """
        for k,v in kwargs.items():
            self.__dict__[k] = v
        self.d = db_manager(self.sim_db_filename)
        self.last_id = self.d.find_last()
        self.add_run()
        self.prep_parent()
        self.determine_times()
        self.create_run_directory()
        self.update()
        self.run()

    def get_params(self):
        params ={}
        #members = [ i for i in dir(self) if (not i.startswith('__')) and not isinstance(eval("self.{}".format(i)), collections.Callable) and (i not in ['d']) ]
        members = [ i for i in dir(self) if (not i.startswith('__')) and (i not in dir(self.__class__)) and (i not in ['d', 'collections', 'randint'])]
        for m in members:
            params[m] = self.__dict__[m]
        return params

    def add_run(self):
        if self.run_type=='continue':
            if self.parent !='last':
                self.run_name = self.d.get(self.parent,'name') #This only makes sure the name matches.  Make sure all your other parameters do, too.
        self.run_id = self.d.add_run(self.run_name)

    def update(self):
        self.d.update(self.run_id,'submitted',"{}".format(datetime.now()))
        #self.d.update(self.run_id,'dir',check_output(["pwd"]))
        self.d.update(self.run_id,'dir',getcwd())
        self.d.update(self.run_id,'continued',int(self.continue_run))
        self.d.update(self.run_id,'new',int(self.initialize_new))
        self.d.update(self.run_id,'system',clusters[self.queue_name])
        self.d.update(self.run_id,'project',self.project)
        self.d.update(self.run_id,'overwrote',0)

    def prep_parent(self):
        if self.parent =='last':
            self.parent_id = self.last_id
        elif self.parent in [None,0]:
            self.parent_id = self.run_id
        elif self.parent=='from_mav':
                self.parent_id = self.infile
        else:
            self.parent_id = self.parent
        if self.parent=='from_mav':
            self.parent_name=self.infile
        else:
            self.parent_name = self.d.get(self.parent_id,'name')

        self.initialize_new=False
        self.randomize_flag=False
        self.shrink_flag   =False
        self.depends_on = None
        self.continue_run = False
        if self.run_type=='new':
            print("making a new run.")
            self.initialize_new =True
            self.randomize_flag=True
            self.shrink_flag=True
        if self.run_type=='continue':
            self.continue_run = True
            if self.d.get(self.parent_id,'tstop') == None:
                print("run hasn't stopped yet")
                self.depends_on = self.parent_id
            print("continuing", self.parent_id)
        if self.run_type=='start_from':
            if self.d.get(self.parent_id,'tstop') == None:
                print("run hasn't stopped yet")
                self.depends_on = self.parent_id
            print("starting from", self.parent_id)
        if self.run_type=='from_mav':
            pass

    def determine_times(self,buf=0.1):
        """Take in desired_hours walltime and calculate resources needed."""
        s = clusters[self.queue_name]
        if max_wall[s] < self.desired_hours:
            print("The selected queue only permits", max_wall[s], "hours, and you want", self.desired_hours)
            exit(1)
        mt = self.mix_time
        etps= self.estimate_tps
        print("Mix time in hours:",mt/etps/3600.)

        init_time = 0.
        if self.initialize_new==True:
            init_time1=0.3
        if self.randomize_flag==True:
            init_time+=(mt/etps)/3600.
        if self.shrink_flag==True:
            init_time+=(mt/etps)/3600.
            if self.ensemble !="NPT":
                init_time+=(mt/etps)/3600.#because non-NPT shrinks currently run for two mix_time periods
        self.stop_hours =  self.desired_hours - init_time - buf
        print("Stop hours: ",self.stop_hours)
        if self.stop_hours < 0.1:
            print("Probably will run into a walltime problem, exiting.")
            exit(1)
        if self.stop_hours + init_time+buf >self.desired_hours:
            print("{} hours needed, but only {} requested.".format(int(self.stop_hours+init_time+buf),self.desired_hours))
            exit(1)
        print("{} hours needed,  {} requested.".format(int(self.stop_hours+init_time+buf),self.desired_hours))
        self.queue_type=None
        if s=='rd':
            self.queue_type="PBS"
            self.resource_str="walltime={}:00:00,nodes=1:ppn={}:gpus=1".format(int(self.desired_hours,self.n_gpus*8))
        if s=='bw':
            self.queue_type="PBS"
            hours = int(self.desired_hours)
            min = int((self.desired_hours - int(self.desired_hours))*60)
            self.resource_str="walltime={}:{}:00,nodes=1:ppn=16:xk".format(hours, min)
        if s=='dirac':
            self.queue_type="PBS"
            self.resource_str="walltime={}:00:00,nodes=1:ppn=8:fermi".format(int(self.desired_hours))
        if s=='kestrel':
            self.queue_type="SLURM"
            self.resource_str="-t {}:00:00".format(int(self.desired_hours))
            #self.resource_str="walltime={}:00:00,select=1:ncpus=8:ngpus=1".format(int(self.desired_hours))
        if s=='maverick':
            self.queue_type="SLURM"
            self.resource_str="-t {}:00:00".format(int(self.desired_hours))
        if s=='virtual-local':
            self.queue_type="virtual-local"
            # This is where we can put virtual env stuff

    def create_run_directory(self):
        """Creates a directory for a new job."""
        if path.exists(self.run_dir)==False:
            makedirs(self.run_dir)
        runs = listdir(self.run_dir)
        if self.initialize_new:
            if self.continue_run:
                print("Hey, I can't both initialize a run and continue one")
                exit(1)
            if self.run_name in runs:
                ans = str(input('OVERWRITE EXISTING RUN?? (Y/N) '))
                if ans != 'Y':
                    print("Not overwriting, exiting...")
                    exit()
                else:
                    self.d.update(self.run_id,'overwrote',1)
        if self.run_name not in runs:
            makedirs(self.run_dir+self.run_name)

    def pbs_script(self):
        """Returns a string for a PBS script."""
        s = clusters[self.queue_name]
        script_str = '#!/bin/bash -l\n'
        if not s=='rd':
                script_str += '#PBS -q {0}\n'.format(self.queue_name)
        if s=='rd':
                script_str += '#PBS -o {}.o\n'.format(self.run_id)
        script_str += '#PBS -N {}\n'.format(self.run_id)
        script_str += '#PBS -j oe \n'
        script_str += '#PBS -l {0}\n'.format(self.resource_str)
        if s  != 'kestrel' and s!= 'bw':
                script_str += '#PBS -l naccesspolicy=singlejob\n'
        script_str += '#PBS -m abe \n'
        script_str += '#PBS -M {0} \n'.format(self.email)
        if self.depends_on !=None:
                script_str += '#PBS -W depend=afterany:{0}\n'.format(self.d.get(self.depends_on,'pbsid'))
                self.d.update(self.run_id,'parent',"{}".format(self.depends_on))
                self.d.update(self.depends_on,'child',"{}".format(self.run_id) )

        if s== 'bw':
                script_str += 'module load cme_utils \n'
        script_str += 'cd {0}\n'.format(self.d.get(self.run_id,'dir'))
        script_str += 'python3 -m cme_utils.job_control.update_db kes {} {} "$(date)"\n'.format(self.run_id,'tstart')
        if s=='bw':
                script_str += 'aprun -n 1 -N 1 -d 1 hoomd 2>&1 {0} | tee {1}\n'.format(self.run_script, self.out)
        else:
                script_str += 'hoomd 2>&1 {0} | tee {1}\n'.format(self.run_script, self.out)
        script_str += 'python3 -m cme_utils.job_control.update_db kes {} {} "$(date)"\n'.format(self.run_id,'tstop')
        script_str += 'python3 -m cme_utils.job_control.get_perf {} \n'.format(self.run_id)
        script_str += 'cp {0} {1}/\n'.format(self.run_script,self.run_dir+self.run_name)
        script_str += 'cp {0} {1}/\n'.format(self.out,self.run_dir+self.run_name)
        script_str += 'mv {0} shells/\n'.format(self.run_script)
        script_str += 'mv {0} shells/\n'.format(self.shell)
        script_str += 'mv {0} shells/\n'.format(self.out)
        return script_str

    def parallel_slurm_script(self):
        """Returns a string to run two jobs at once on Kestrel"""
        script_str = '#!/bin/bash -l\n'
        script_str += '#SBATCH -p {0}\n'.format(self.queue_name)
        script_str += '#SBATCH -J {0}-{1}\n'.format(self.run_id,self.run_name)
        script_str += '#SBATCH -o {0}.o%j\n'.format(self.run_name)
        script_str += '#SBATCH -N 1\n'
        script_str += '#SBATCH -n 2\n'
        script_str += '#SBATCH --mail-type=All\n'
        script_str += '#SBATCH --mail-user={0}\n'.format(self.email)
        script_str += '#SBATCH {}\n'.format(self.resource_str)

        s = clusters[self.queue_name]
        script_str += '#SBATCH --exclusive\n'
        script_str += '#SBATCH --gres=gpu:2\n'
        dbn = 'kes'

        #add dependencies here

        script_str += 'cd {0}\n'.format(self.d.get(self.run_id,'dir'))
        script_str += 'python3 -m cme_utils.job_control.update_db {} {} {} "$(date)"\n'.format(dbn,self.run_id,'tstart')
        script_str += 'srun --mpi=pmi2 --kill-on-bad-exit --cpu_bind=map_cpu:8 -n1 python3 2>&1 {0} | tee {1}\n'.format(self.run_script, self.out)
        script_str += 'python3 -m cme_utils.job_control.update_db {} {} {} "$(date)"\n'.format(dbn,self.run_id,'tstop')
        script_str += 'python3 -m cme_utils.job_control.get_perf {} {} \n'.format(dbn,self.run_id)
        script_str += 'cp {0} {1}/\n'.format(self.run_script,self.run_dir+self.run_name)
        script_str += 'cp {0} {1}/\n'.format(self.out,self.run_dir+self.run_name)
        script_str += 'mv {0} shells/\n'.format(self.run_script)
        script_str += 'mv {0} shells/\n'.format(self.shell)
        script_str += 'mv {0} shells/\n'.format(self.out)
        return script_str

    def slurm_script(self):
        """Returns a string for a SLURM script."""
        script_str = '#!/bin/bash -l\n'
        script_str += '#SBATCH -p {0}\n'.format(self.queue_name)
        script_str += '#SBATCH -J {0}-{1}\n'.format(self.run_id,self.run_name)
        script_str += '#SBATCH -o {0}.o%j\n'.format(self.run_name)
        script_str += '#SBATCH -N 1\n'
        script_str += '#SBATCH -n 1\n'
        script_str += '#SBATCH --mail-type=All\n'
        script_str += '#SBATCH --mail-user={0}\n'.format(self.email)
        script_str += '#SBATCH {}\n'.format(self.resource_str)

        s = clusters[self.queue_name]
        if s=='maverick':
            script_str += '#SBATCH -A {}\n'.format(self.allocation)
            dbn = 'mav'
        if s=='kestrel':
            #script_str += '#SBATCH -A {}\n'.format(self.username)
            script_str += '#SBATCH --exclusive\n'
            script_str += '#SBATCH --gres=gpu:1\n'
            dbn = 'kes'

        if self.depends_on != None:
            script_str += '#SBATCH --dependency=afterok:{0}\n'.format(self.d.get(self.depends_on,'pbsid'))
            self.d.update(self.run_id,'parent',"{}".format(self.depends_on) )
            self.d.update(self.depends_on,'child',"{}".format(self.run_id) )
        script_str += 'cd {0}\n'.format(self.d.get(self.run_id,'dir'))
        script_str += 'python3 -m cme_utils.job_control.update_db {} {} {} "$(date)"\n'.format(dbn,self.run_id,'tstart')
        if s=='maverick':
            script_str += 'hoomd 2>&1 {0} | tee {1}\n'.format(self.run_script, self.out)
        if s=='kestrel':
            #script_str += 'hoomd 2>&1 {0} | tee {1}\n'.format(self.run_script, self.out)
            script_str += 'srun --mpi=pmi2 --kill-on-bad-exit --cpu_bind=map_cpu:8 -n1 python3 2>&1 {0} | tee {1}\n'.format(self.run_script, self.out)
            #script_str += 'srun --mpi=pmi2 --kill-on-bad-exit --cpu_bind=map_cpu:8,9,10,11,12,13,14,15 -n 8 hoomd 2>&1 {0} --mode=gpu | tee {1}\n'.format(self.run_script, self.out)
        script_str += 'python3 -m cme_utils.job_control.update_db {} {} {} "$(date)"\n'.format(dbn,self.run_id,'tstop')
        script_str += 'python3 -m cme_utils.job_control.get_perf {} {} \n'.format(dbn,self.run_id)
        script_str += 'cp {0} {1}/\n'.format(self.run_script,self.run_dir+self.run_name)
        script_str += 'cp {0} {1}/\n'.format(self.out,self.run_dir+self.run_name)
        script_str += 'mv {0} shells/\n'.format(self.run_script)
        script_str += 'mv {0} shells/\n'.format(self.shell)
        script_str += 'mv {0} shells/\n'.format(self.out)
        return script_str

    def local_script(self):
        """Returns a string for a local job script."""
        script_str = '#!/bin/bash -l\n'
        script_str += 'cd {0}\n'.format(self.d.get(self.run_id,'dir'))
        script_str += 'python3 -m cme_utils.job_control.update_db kes {} {} "$(date)"\n'.format(self.run_id,'tstart')
        script_str += 'python3 {0} 2>&1 | tee {1}\n'.format(self.run_script, self.out)
        script_str += 'python3 -m cme_utils.job_control.update_db kes {} {} "$(date)"\n'.format(self.run_id,'tstop')
        script_str += 'python3 -m cme_utils.job_control.get_perf kes {} \n'.format(self.run_id)
        script_str += 'cp {0} {1}/\n'.format(self.run_script,self.run_dir+self.run_name)
        script_str += 'cp {0} {1}/\n'.format(self.out,self.run_dir+self.run_name)
        script_str += 'mv {0} shells/\n'.format(self.run_script)
        script_str += 'mv {0} shells/\n'.format(self.shell)
        script_str += 'mv {0} shells/\n'.format(self.out)
        return script_str

    def virtual_script(self):
        """Returns a string for a local job script.
        and understands conda envs"""
        script_str = '#!/usr/bin/env bash\n'
        script_str += 'echo test\n'
        script_str += 'source activate hoomd2-python3\n'
        script_str += 'echo `which python3`\n'
        script_str += 'cd {0}\n'.format(self.d.get(self.run_id,'dir'))
        script_str += 'python3 -m cme_utils.job_control.update_db kes {} {} "$(date)"\n'.format(self.run_id,'tstart')
        script_str += 'python3 {0} 2>&1 | tee {1}\n'.format(self.run_script, self.out)
        script_str += 'python3 -m cme_utils.job_control.update_db kes {} {} "$(date)"\n'.format(self.run_id,'tstop')
        script_str += 'python3 -m cme_utils.job_control.get_perf kes {} \n'.format(self.run_id)
        script_str += 'cp {0} {1}/\n'.format(self.run_script,self.run_dir+self.run_name)
        script_str += 'cp {0} {1}/\n'.format(self.out,self.run_dir+self.run_name)
        script_str += 'mv {0} shells/\n'.format(self.run_script)
        script_str += 'mv {0} shells/\n'.format(self.shell)
        script_str += 'mv {0} shells/\n'.format(self.out)
        return script_str

    def gen_hoomd_job(self,filename):
        """Generates the python script that hoomd will be invoked with."""
        job_string = "o = opv_run("
        for p in self.get_params():
            job_string += "{} = {!r}, ".format(p,self.__dict__[p])
        job_string = job_string[:-2]+")\no.run()\n"
        with open(self.run_script,'w') as rf:
            with open(filename,'r') as tf:
                rf.write(tf.read())
                rf.write(job_string)

    def submit(self):
        """Submits job to machine."""
        if self.queue_type == "SLURM":
            job = check_output(["sbatch",self.shell], universal_newlines=True)
            print(job)
            jobid = job.split()[-1]
            self.d.update(self.run_id,'pbsid',"{}".format(jobid))
        if self.queue_type == "PBS":
            job = check_output(["qsub",self.shell], universal_newlines=True)
            jobid = job.split('.')[0]
            self.d.update(self.run_id,'pbsid',"{}".format(jobid))
        if self.queue_type == None:
            print(self.shell)
            call("bash {0} 2>&1 | tee {1:06d}.o".format(self.shell,self.run_id),shell=True)

    def run(self):
        """Main entry point."""
        self.shell="{0:06d}.sh".format(self.run_id)
        self.run_script="{0:06d}.py".format(self.run_id)
        self.out="{0:06d}.o".format(self.run_id)
        self.last_submit = self.d.find_last_submitted()
        self.gen_hoomd_job("opv.py")
        if self.queue_type == "SLURM":
            write(self.shell,self.slurm_script())
        if self.queue_type == "PBS":
            write(self.shell,self.pbs_script())
        if self.queue_type == "virtual-local":
            print("Virtual-Local")
            write(self.shell,self.virtual_script())
        if self.queue_type == None:
            write(self.shell,self.local_script())
        if self.submit_job ==True:
            self.submit()

